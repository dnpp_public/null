package null

import (
	"bytes"
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"os"
	"time"
)

// Time is a nullable time.Time. It supports SQL and JSON serialization.
// It will marshal to null if null.
type TimeSkipErr struct {
	sql.NullTime
}

// Value implements the driver Valuer interface.
func (t TimeSkipErr) Value() (driver.Value, error) {
	if !t.Valid {
		return nil, nil
	}
	return t.Time, nil
}

// NewTime creates a new Time.
func NewTimeSkipErr(t time.Time, valid bool) Time {
	return Time{
		NullTime: sql.NullTime{
			Time:  t,
			Valid: valid,
		},
	}
}

// TimeFrom creates a new Time that will always be valid.
func TimeSkipErrFrom(t time.Time) Time {
	return NewTime(t, true)
}

// TimeFromPtr creates a new Time that will be null if t is nil.
func TimeSkipErrFromPtr(t *time.Time) Time {
	if t == nil {
		return NewTime(time.Time{}, false)
	}
	return NewTime(*t, true)
}

// ValueOrZero returns the inner value if valid, otherwise zero.
func (t TimeSkipErr) ValueOrZero() time.Time {
	if !t.Valid {
		return time.Time{}
	}
	return t.Time
}

// MarshalJSON implements json.Marshaler.
// It will encode null if this time is null.
func (t TimeSkipErr) MarshalJSON() ([]byte, error) {
	if !t.Valid {
		return []byte("null"), nil
	}
	return t.Time.MarshalJSON()
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports string and null input.
func (t *TimeSkipErr) UnmarshalJSON(data []byte) error {
	if bytes.Equal(data, nullBytes) {
		t.Valid = false
		return nil
	}

	if err := json.Unmarshal(data, &t.Time); err != nil {
		return fmt.Errorf("null: couldn't unmarshal JSON: %w", err)
	}

	t.Valid = true
	return nil
}

// MarshalText implements encoding.TextMarshaler.
// It returns an empty string if invalid, otherwise time.Time's MarshalText.
func (t TimeSkipErr) MarshalText() ([]byte, error) {
	if !t.Valid {
		return []byte{}, nil
	}
	return t.Time.MarshalText()
}

// UnmarshalText implements encoding.TextUnmarshaler.
// It has backwards compatibility with v3 in that the string "null" is considered equivalent to an empty string
// and unmarshaling will succeed. This may be removed in a future version.
func (t *TimeSkipErr) UnmarshalText(text []byte) error {
	str := string(text)
	// allowing "null" is for backwards compatibility with v3
	if str == "" || str == "null" {
		t.Valid = false
		return nil
	}
	if err := t.Time.UnmarshalText(text); err != nil {
		val, err := time.Parse("2006-01-02T15:04:05", str)
		if err != nil {
			filename := "TimeSkipErr_exceptions.txt"
			var file *os.File
			_, err := os.Stat(filename)
			if os.IsNotExist(err) { // create file if not exists
				file, err = os.Create(filename)
				if err != nil {
					return err
				}
			} else {
				file, err = os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
				if err != nil {
					return err
				}
			}

			defer file.Close()
			fmt.Fprintf(file, "null: couldn't unmarshal time: %s", string(text)+"\n")

			t.Valid = false
			return nil
		} else {
			t.Time = val
		}
	}
	t.Valid = true
	return nil
}

// Set changes this Time's value and sets it to be non-null.
func (t *TimeSkipErr) Set(v time.Time) {
	t.Time = v
	t.Valid = true
}

// Ptr returns a pointer to this Time's value, or a nil pointer if this Time is null.
func (t TimeSkipErr) Ptr() *time.Time {
	if !t.Valid {
		return nil
	}
	return &t.Time
}

// IsZero returns true for invalid Times, hopefully for future omitempty support.
// A non-null Time with a zero value will not be considered zero.
func (t TimeSkipErr) IsZero() bool {
	return !t.Valid
}

// Equal returns true if both Time objects encode the same time or are both null.
// Two times can be equal even if they are in different locations.
// For example, 6:00 +0200 CEST and 4:00 UTC are Equal.
func (t TimeSkipErr) Equal(other Time) bool {
	return t.Valid == other.Valid && (!t.Valid || t.Time.Equal(other.Time))
}

// ExactEqual returns true if both Time objects are equal or both null.
// ExactEqual returns false for times that are in different locations or
// have a different monotonic clock reading.
func (t TimeSkipErr) ExactEqual(other Time) bool {
	return t.Valid == other.Valid && (!t.Valid || t.Time == other.Time)
}
