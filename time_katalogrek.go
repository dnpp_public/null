package null

import (
	"bytes"
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

// Time is a nullable time.Time. It supports SQL and JSON serialization.
// It will marshal to null if null.
type TimeKatalogrek struct {
	sql.NullTime
}

// Value implements the driver Valuer interface.
func (t TimeKatalogrek) Value() (driver.Value, error) {
	if !t.Valid {
		return nil, nil
	}
	return t.Time, nil
}

// NewTime creates a new Time.
func NewTimeKatalogrek(t time.Time, valid bool) TimeKatalogrek {
	return TimeKatalogrek{
		NullTime: sql.NullTime{
			Time:  t,
			Valid: valid,
		},
	}
}

// TimeFrom creates a new Time that will always be valid.
func TimeKatalogrekFrom(t time.Time) TimeKatalogrek {
	return NewTimeKatalogrek(t, true)
}

// TimeFromPtr creates a new Time that will be null if t is nil.
func TimeKatalogrekFromPtr(t *time.Time) TimeKatalogrek {
	if t == nil {
		return NewTimeKatalogrek(time.Time{}, false)
	}
	return NewTimeKatalogrek(*t, true)
}

// ValueOrZero returns the inner value if valid, otherwise zero.
func (t TimeKatalogrek) ValueOrZero() time.Time {
	if !t.Valid {
		return time.Time{}
	}
	return t.Time
}

// MarshalJSON implements json.Marshaler.
// It will encode null if this time is null.
func (t TimeKatalogrek) MarshalJSON() ([]byte, error) {
	if !t.Valid {
		return []byte("null"), nil
	}
	return t.Time.MarshalJSON()
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports string and null input.
func (t *TimeKatalogrek) UnmarshalJSON(data []byte) error {
	if bytes.Equal(data, nullBytes) {
		t.Valid = false
		return nil
	}

	if err := json.Unmarshal(data, &t.Time); err != nil {
		strVal := strings.ReplaceAll(string(data), `"`, ``)
		val, err_parse := time.Parse("02.01.2006 15:04:05.999999999", strVal)
		if err_parse != nil {
			return fmt.Errorf("null: couldn't unmarshal JSON time: %w", err_parse)
		} else {
			t.Time = val
		}
	}

	t.Valid = true
	return nil
}

// MarshalText implements encoding.TextMarshaler.
// It returns an empty string if invalid, otherwise time.Time's MarshalText.
func (t TimeKatalogrek) MarshalText() ([]byte, error) {
	if !t.Valid {
		return []byte{}, nil
	}
	return t.Time.MarshalText()
}

// UnmarshalText implements encoding.TextUnmarshaler.
// It has backwards compatibility with v3 in that the string "null" is considered equivalent to an empty string
// and unmarshaling will succeed. This may be removed in a future version.
func (t *TimeKatalogrek) UnmarshalText(text []byte) error {
	str := string(text)
	// allowing "null" is for backwards compatibility with v3
	if str == "" || str == "null" {
		t.Valid = false
		return nil
	}
	if err := t.Time.UnmarshalText(text); err != nil {
		val, err := time.Parse("02.01.2006 15:04:05.999999999", str)
		if err != nil {
			return fmt.Errorf("null: couldn't unmarshal time: %w", err)
		} else {
			t.Time = val
		}
	}
	t.Valid = true
	return nil
}

// Set changes this Time's value and sets it to be non-null.
func (t *TimeKatalogrek) Set(v time.Time) {
	t.Time = v
	t.Valid = true
}

// Ptr returns a pointer to this Time's value, or a nil pointer if this Time is null.
func (t TimeKatalogrek) Ptr() *time.Time {
	if !t.Valid {
		return nil
	}
	return &t.Time
}

// IsZero returns true for invalid Times, hopefully for future omitempty support.
// A non-null Time with a zero value will not be considered zero.
func (t TimeKatalogrek) IsZero() bool {
	return !t.Valid
}

// Equal returns true if both Time objects encode the same time or are both null.
// Two times can be equal even if they are in different locations.
// For example, 6:00 +0200 CEST and 4:00 UTC are Equal.
func (t TimeKatalogrek) Equal(other Time) bool {
	return t.Valid == other.Valid && (!t.Valid || t.Time.Equal(other.Time))
}

// ExactEqual returns true if both Time objects are equal or both null.
// ExactEqual returns false for times that are in different locations or
// have a different monotonic clock reading.
func (t TimeKatalogrek) ExactEqual(other Time) bool {
	return t.Valid == other.Valid && (!t.Valid || t.Time == other.Time)
}
