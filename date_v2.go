package null

import (
	"database/sql"
	"fmt"
	"time"
)

// Date is a nullable Date. It supports SQL and JSON serialization.
// It will marshal to null if null.
type Date_v2 struct {
	sql.NullTime
}

// NewDate creates a new Date.
func NewDate_v2(t time.Time, valid bool) Date_v2 {
	return Date_v2{
		NullTime: sql.NullTime{
			Time:  t,
			Valid: valid,
		},
	}
}

// DateFrom creates a new Time that will always be valid.
func Date_v2From(t time.Time) Date_v2 {
	return NewDate_v2(t, true)
}

// UnmarshalText implements encoding.TextUnmarshaler.
// It has backwards compatibility with v3 in that the string "null" is considered equivalent to an empty string
// and unmarshaling will succeed. This may be removed in a future version.
func (t *Date_v2) UnmarshalText(text []byte) error {
	str := string(text)
	// allowing "null" is for backwards compatibility with v3
	if str == "" || str == "null" {
		t.Valid = false
		return nil
	}
	date, err := time.Parse(`02.01.2006`, string(text))
	if err != nil {
		return fmt.Errorf("null: couldn't unmarshal text: %w", err)
	}
	t.Time = date
	t.Valid = true
	return nil
}

func (t *Date_v2) GetYear() sql.NullInt64 {
	var yearVal sql.NullInt64
	if t.Valid {
		yearVal.Int64 = int64(t.Time.Year())
		yearVal.Valid = true
	} else {
		yearVal.Int64 = 0
		yearVal.Valid = false
	}
	return yearVal
}

func (t *Date_v2) String() string {
	var str string
	if t.Valid {
		str = "'" + t.Time.Format("02.01.2006") + "'"
	} else {
		str = "NULL"
	}
	return str
}
