// Package null contains SQL types that consider zero input and null input as separate values,
// with convenient support for JSON and text marshaling.
// Types in this package will always encode to their null value if null.
// Use the zero subpackage if you want zero values and null to be treated the same.
package null

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"strings"

	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

// String is a nullable string. It supports SQL and JSON serialization.
// It will marshal to null if null. Blank string input will be considered null.
type String_win1251 struct {
	sql.NullString
}

// StringFrom creates a new String that will never be blank.
func String_win1251From(s string) String_win1251 {
	return NewString_win1251(s, strings.TrimSpace(s) != "")
}

// StringFromPtr creates a new String that be null if s is nil.
func StringFromPtr_win1251(s *string) String_win1251 {
	if s == nil {
		return NewString_win1251("", false)
	}
	return NewString_win1251(*s, true)
}

// ValueOrZero returns the inner value if valid, otherwise zero.
func (s String_win1251) ValueOrZero() string {
	if !s.Valid {
		return ""
	}
	return s.String
}

// NewString creates a new String
func NewString_win1251(s string, valid bool) String_win1251 {
	return String_win1251{
		NullString: sql.NullString{
			String: s,
			Valid:  valid,
		},
	}
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports string and null input. Blank string input does not produce a null String.
func (s *String_win1251) UnmarshalJSON(data []byte) error {
	if bytes.Equal(data, nullBytes) {
		s.Valid = false
		return nil
	}

	if err := json.Unmarshal(data, &s.String); err != nil {
		if err.Error() == `json: cannot unmarshal number into Go value of type string` {
			s.String = string(data)
		} else if ok, val := IsArrayOfVals(data); ok {
			s.String = val
		} else {
			return fmt.Errorf("null: couldn't unmarshal JSON: %w", err)
		}
	}

	s.String = strings.TrimSpace(s.String)
	s.Valid = s.String != ""
	return nil
}

// MarshalJSON implements json.Marshaler.
// It will encode null if this String is null.
func (s String_win1251) MarshalJSON() ([]byte, error) {
	if !s.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(s.String)
}

// MarshalText implements encoding.TextMarshaler.
// It will encode a blank string when this String is null.
func (s String_win1251) MarshalText() ([]byte, error) {
	if !s.Valid {
		return []byte{}, nil
	}
	return []byte(s.String), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
// It will unmarshal to a null String if the input is a blank string.
func (s *String_win1251) UnmarshalText(text []byte) error {

	reader := transform.NewReader(bytes.NewReader(text), charmap.Windows1251.NewDecoder())
	valueConverted, err := ioutil.ReadAll(reader)

	if err != nil {
		return err
	}

	s.String = strings.TrimSpace(string(valueConverted))
	s.Valid = s.String != ""
	return nil
}

// Set changes this String's value and also sets it to be non-null.
func (s *String_win1251) Set(v string) {
	s.String = v
	s.Valid = true
}

// Ptr returns a pointer to this String's value, or a nil pointer if this String is null.
func (s String_win1251) Ptr() *string {
	if !s.Valid {
		return nil
	}
	return &s.String
}

// IsZero returns true for null strings, for potential future omitempty support.
func (s String_win1251) IsZero() bool {
	return !s.Valid
}

// Equal returns true if both strings have the same value or are both null.
func (s String_win1251) Equal(other String_win1251) bool {
	return s.Valid == other.Valid && (!s.Valid || s.String == other.String)
}

// Scan implements the Scanner interface for NullString
func (s *String_win1251) Scan(value interface{}) error {
	var buff sql.NullString
	if err := buff.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		s.String = string(buff.String)
		s.Valid = false

	} else {
		s.String = string(buff.String)
		s.Valid = s.String != ""
	}

	return nil
}
