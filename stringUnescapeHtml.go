// Package null contains SQL types that consider zero input and null input as separate values,
// with convenient support for JSON and text marshaling.
// Types in this package will always encode to their null value if null.
// Use the zero subpackage if you want zero values and null to be treated the same.
package null

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"html"
)

// nullBytes is a JSON null literal
//var nullBytes = []byte("null")

// Unmarshall html codes to cyrilic str
// e.g. &#x41A;&#x418;&#x420;&#x418;&#x41B;&#x41B;  -> КИРИЛЛ
// String is a nullable string. It supports SQL and JSON serialization.
// It will marshal to null if null. Blank string input will be considered null.
type StringUnescapeHtml struct {
	sql.NullString
}

// StringFrom creates a new String that will never be blank.
func StringUnescapeHtmlFrom(s string) StringUnescapeHtml {
	return NewStringUnescapeHtml(s, s != "")
}

// StringFromPtr creates a new String that be null if s is nil.
func StringUnescapeHtmlFromPtr(s *string) StringUnescapeHtml {
	if s == nil {
		return NewStringUnescapeHtml("", false)
	}
	return NewStringUnescapeHtml(*s, true)
}

// ValueOrZero returns the inner value if valid, otherwise zero.
func (s StringUnescapeHtml) ValueOrZero() string {
	if !s.Valid {
		return ""
	}
	return s.String
}

// NewString creates a new String
func NewStringUnescapeHtml(s string, valid bool) StringUnescapeHtml {
	return StringUnescapeHtml{
		NullString: sql.NullString{
			String: s,
			Valid:  valid,
		},
	}
}

// UnmarshalJSON implements json.Unmarshaler.
// It supports StringUnescapeHtml and null input. Blank string input does not produce a null String.
func (s *StringUnescapeHtml) UnmarshalJSON(data []byte) error {
	if bytes.Equal(data, nullBytes) {
		s.Valid = false
		return nil
	}

	if err := json.Unmarshal(data, &s.String); err != nil {
		return fmt.Errorf("null: couldn't unmarshal JSON: %w", err)
	}

	s.Valid = true
	return nil
}

// MarshalJSON implements json.Marshaler.
// It will encode null if this StringUnescapeHtml is null.
func (s StringUnescapeHtml) MarshalJSON() ([]byte, error) {
	if !s.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(s.String)
}

// MarshalText implements encoding.TextMarshaler.
// It will encode a blank string when this StringUnescapeHtml is null.
func (s StringUnescapeHtml) MarshalText() ([]byte, error) {
	if !s.Valid {
		return []byte{}, nil
	}
	return []byte(s.String), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
// It will unmarshal to a null String if the input is a blank string.
func (s *StringUnescapeHtml) UnmarshalText(text []byte) error {
	s.String = html.UnescapeString(string(text))
	s.Valid = s.String != ""
	return nil
}

// Set changes this StringUnescapeHtml's value and also sets it to be non-null.
func (s *StringUnescapeHtml) Set(v string) {
	s.String = v
	s.Valid = true
}

// Ptr returns a pointer to this StringUnescapeHtml's value, or a nil pointer if this StringUnescapeHtml is null.
func (s StringUnescapeHtml) Ptr() *string {
	if !s.Valid {
		return nil
	}
	return &s.String
}

// IsZero returns true for null strings, for potential future omitempty support.
func (s StringUnescapeHtml) IsZero() bool {
	return !s.Valid
}

// Equal returns true if both strings have the same value or are both null.
func (s StringUnescapeHtml) Equal(other StringUnescapeHtml) bool {
	return s.Valid == other.Valid && (!s.Valid || s.String == other.String)
}
